﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
using RestSharp.Extensions;
using System.IO;
using Docitt.UserProfile.Utility;

namespace Docitt.UserProfile.Client
{
    public class UserProfileService : IUserProfileService
    {
        private IServiceClient Client { get; }

        public UserProfileService(IServiceClient client)
        {
            Client = client;
        }

        public async Task<IUserProfile> GetProfile(string username)
        {
            return await Client.GetAsync<UserProfile>($"/{username}");
        }

        public async Task<IUserProfile> AddProfile(IUserProfile profile)
        {
             return await Client.PostAsync<IUserProfile, UserProfile>("/", profile, true);
        }
        
        public async Task<IDocument> UploadLogo(string entityType, string entityId, LogoDetails logoDetail)
        {
            return await Client.PostAsync<LogoDetails, Document>($"/{entityType}/{entityId}/upload-logo", logoDetail, true);
        }

        public async Task<LogoDetails> GetLogo(string entityType, string entityId)
        {
           return await Client.GetAsync<LogoDetails>($"/{entityType}/{entityId}/logo");
        }

        public async Task<IEnumerable<UserRelationship>> UpdateUserRelationship(string username,List<UserRelationship> relationships)
        {
            return await Client.PutAsync<List<UserRelationship>, List<UserRelationship>>($"/{username}/relationships", relationships, true);
        }

        public async Task<IEnumerable<UserRelationship>> GetUserRelationships(string username)
        {
            return await Client.GetAsync<List<UserRelationship>>($"/{username}/relationships/all");
        }

        public async Task<IUserProfile> UpdateProfile(string username, IUserProfile profile)
        {
            return await Client.PutAsync<IUserProfile, UserProfile>($"/{username}", profile, true);
        }

        public async Task<UserAccessModel> UpdateUserAccess(string username, UserAccessModel relationships)
        {   
            return await Client.PutAsync<UserAccessModel, UserAccessModel>($"/{username}/access", relationships, true);
        }

        public async Task<UserAccessModel> GetUserAccess(string username)
        {
            return await Client.GetAsync<UserAccessModel>($"/{username}/access");
        }

        public async Task<IGroupMemberResponse> AddGroupMembers(string entityType, string entityId, PostedFileDetails file)
        {
            var request = new RestRequest("/{entityType}/{entityId}/group-members/import", Method.PUT);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);

            if (file != null)
            {
                request.AddFile("file", file.Content.ReadAsBytes(), "file.csv", MimeMapping.GetMimeMapping(Path.GetExtension("file.csv")));
            }

            return await Client.ExecuteAsync<GroupMemberResponse>(request);
        }

        public async Task<IIndividualMemberResponse> AddIndividualMember(string entityType, string entityId, UserProfile member)
        {
            return await Client.PutAsync<UserProfile, IndividualMemberResponse>($"/{entityType}/{entityId}/member", member, true);
        }

        public async Task<ISocialPlatformModel> UpdateSocialPlatform(string username, SocialPlatformModel socialPlatforms)
        {
            return await Client.PutAsync<SocialPlatformModel, SocialPlatformModel>($"/{username}/social-platform", socialPlatforms, true);
        }

        public async Task<object> UpdateSettings(string username, string key, object value)
        {
            return await Client.PutAsync<object, object>($"/{username}/setting/{key}/update", value, true);
        }

        public async Task<object> GetSetting(string username, string key)
        {
            return await Client.GetAsync<object>($"/{username}/setting/{key}");
        }

        public async Task<Dictionary<string, object>> GetAllSettings(string username)
        {
            return await Client.GetAsync<Dictionary<string, object>>($"/{username}/setting/all");
        }

        public async Task<bool> EnableDashboard(string username)
        {
            var userName = username;
            var response = await Client.PutAsync<dynamic, dynamic>($"/user/{userName}/enable-dashboard", null, true);
            return Convert.ToBoolean(response);
        }

        public async Task<ILoanOfficer> GetLoanOfficer(string email)
        {
            var loanOfficerEmail =email;
            return await Client.GetAsync<LoanOfficer>($"/user/loanofficer/{loanOfficerEmail}/brandedsiteurl");
        }

        public async Task<ILoanOfficerUniqueCodeResponse> GetProfileByUniqueCode(string brandedUniqueCode)
        {
            return await Client.GetAsync<LoanOfficerUniqueCodeResponse>($"/user/brandedcode/{brandedUniqueCode}");
        }

        public async Task<IVerifyBrandedSiteTokenResponse> VerifyBrandedSiteToken(string brandedSiteToken)
        {
            return await Client.GetAsync<VerifyBrandedSiteTokenResponse>($"/user/loanofficer/{brandedSiteToken}/verifybrandedsitetoken");
        }


        public async Task<IUserProfile> GetProfile(string firstname, string lastname, string email)
        {
            return await Client.GetAsync<UserProfile>($"/user/fname/{firstname}/lname/{lastname}/email/{email}");
        }

        public async Task<List<IUserAutoSuggestion>> GetLoanOfficerList()
        {
            return await Client.GetAsync<List<IUserAutoSuggestion>>($"/users/lo");
        }
    }
}