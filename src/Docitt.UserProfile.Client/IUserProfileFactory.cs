﻿using LendFoundry.Security.Tokens;

namespace Docitt.UserProfile.Client
{
    public interface IUserProfileFactory
    {
        IUserProfileService Create(ITokenReader reader);
    }
}