﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace Docitt.UserProfile.Client
{
    public static class UserProfileServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddUserProfileService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IUserProfileFactory>(p => new UserProfileFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IUserProfileFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddUserProfileService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IUserProfileFactory>(p => new UserProfileFactory(p, uri));
            services.AddTransient(p => p.GetService<IUserProfileFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddUserProfileService(this IServiceCollection services)
        {
            services.AddTransient<IUserProfileFactory>(p => new UserProfileFactory(p));
            services.AddTransient(p => p.GetService<IUserProfileFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

       
    }
}