﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface IUserProfileRepository : IRepository<IUserProfile>
    {
        Task<IUserProfile> GetByUsername(string username);

        Task<IUserProfile> GetByEmail(string email);

        Task<IUserProfile> GetById(string uid);

        Task<IUserProfile> UpdateProfile(string username, IUserProfile userProfile);

        Task<bool> UpdateUserProfileCompanyInfo(string username, string companyId);

        Task<bool> UpdateEnableDashboard(string userName);

        Task<IUserProfile> GetByUniqueCode(string brandedUniqueCode);

        Task<string> GetBrandedUniqueCode();

        Task<IUserProfile> GetProfile(string firstname, string lastname, string email);

        Task<List<IUserAutoSuggestion>> GetUserListByTitle(string title);
    }
}