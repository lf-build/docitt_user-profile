﻿
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface IUserCompanyProfileService
    {
        Task<IUserCompanyProfile> GetCompanyProfile(string companyId, string searchkey);

        Task<IUserCompanyProfile> AddCompanyProfile(IUserCompanyProfile companyProfile);

        Task<bool> UpdateCompanyProfileInfo(IUserCompanyProfile companyProfile);

        Task<bool> UpdateUserProfileCompanyInfo(string companyId);
    }
}