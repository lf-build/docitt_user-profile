﻿using Docitt.UserProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public class GroupMemberResponse : IGroupMemberResponse
    {
        public GroupMemberResponse()
        {
            SucceededMembers = new List<Docitt.UserProfile.IUserProfile>();
            InvalidMembers = new List<IUserProfile>();
            DuplicatedMembers = new List<IUserProfile>();
        }
        public List<IUserProfile> SucceededMembers { get; set; }
        public List<IUserProfile> InvalidMembers { get; set; }
        public List<IUserProfile> DuplicatedMembers { get; set; }
    }
}
