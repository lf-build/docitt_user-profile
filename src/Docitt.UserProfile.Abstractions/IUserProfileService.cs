﻿using LendFoundry.DocumentManager;
using System.Collections.Generic;

using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface IUserProfileService
    {
        Task<IUserProfile> GetProfile(string userName);

        Task<IUserProfile> AddProfile(IUserProfile profile);

        Task<IUserProfile> UpdateProfile(string username, IUserProfile profile);

        Task<IDocument> UploadLogo(string entityType, string entityId, LogoDetails logoDetail);

        Task<LogoDetails> GetLogo(string entityType, string entityId);

        Task<IEnumerable<UserRelationship>> UpdateUserRelationship(string username, List<UserRelationship> relationships);

        Task<IEnumerable<UserRelationship>> GetUserRelationships(string username);

        Task<UserAccessModel> UpdateUserAccess(string username, UserAccessModel userProfileSettings);

        Task<UserAccessModel> GetUserAccess(string username);

        Task<IGroupMemberResponse> AddGroupMembers(string entityType, string entityId, PostedFileDetails file);

        Task<IIndividualMemberResponse> AddIndividualMember(string entityType, string entityId, UserProfile member);

        Task<ISocialPlatformModel> UpdateSocialPlatform(string username, SocialPlatformModel socialPlatforms);

        Task<object> UpdateSettings(string username, string key, object value);

        Task<object> GetSetting(string username, string key);

        Task<Dictionary<string, object>> GetAllSettings(string username);

        Task<bool> EnableDashboard(string username);

        Task<ILoanOfficer> GetLoanOfficer(string email);

        Task<ILoanOfficerUniqueCodeResponse> GetProfileByUniqueCode(string brandedUniqueCode);
        
        Task<IVerifyBrandedSiteTokenResponse> VerifyBrandedSiteToken(string brandedUniqueCode);

        Task<IUserProfile> GetProfile(string fname, string lname, string email);

        Task<List<IUserAutoSuggestion>> GetLoanOfficerList();
    }
}