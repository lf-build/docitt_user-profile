﻿namespace Docitt.UserProfile
{
    public class LoanOfficerUniqueCodeResponse : ILoanOfficerUniqueCodeResponse
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string BrandedUniqueCode { get; set; }
        public string NmlsNumber { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string MobileNumber { get; set; }
    }
}