﻿namespace Docitt.UserProfile
{
    public interface ILoanOfficerUniqueCodeResponse
    {
        string Username { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }
        string BrandedUniqueCode { get; set; }
        string NmlsNumber { get; set; }
        string Title { get; set; }
        string Phone { get; set; }
        string MobileNumber { get; set; }
    }
}