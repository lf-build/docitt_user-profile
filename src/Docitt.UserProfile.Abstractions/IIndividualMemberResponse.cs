﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface IIndividualMemberResponse
    {
        bool IsValid { get; }
        string Message { get; set; }
        IUserProfile Member { get; set; }
    }
}
