﻿namespace Docitt.UserProfile.Events
{
    public class UserProfileCreated
    {
        public IUserProfile UserProfile { get; set; }
    }
}