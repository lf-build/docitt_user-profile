﻿namespace Docitt.UserProfile.Events
{
    public class UserEvent
    {
        public UserEvent(string username)
        {
            Username = username;
        }

        public string Username { get; set; }
    }
}