﻿namespace Docitt.UserProfile.Events
{
    public class UserProfileModified
    {
        public IUserProfile UserProfile { get; set; }
    }
}