﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public enum LoanEventNotification
    {
        DoNotSend = 1,
        SendText = 2,
        SendEmail = 3,
        SendEmailAndText = 4
    }
}
