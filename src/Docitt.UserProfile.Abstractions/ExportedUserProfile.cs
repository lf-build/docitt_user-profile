﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public class ExportedUserProfile : IExportedUserProfile
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Title { get; set; }
        public string License { get; set; }
        public string MobileNumber { get; set; }
        public string OfficePhone { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string LinkedInUrl { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
        public PreferredCommunication Communication { get; set; } = new PreferredCommunication() { Email = true, Phone = true, Text = true };
        public string BranchName { get; set; }
        public string Note { get; set; }
        public List<UserRelationship> Relationships { get; set; }
        public Dictionary<string, object> Settings { get; set; }
        public string LoanEventNotification { get; set; }

        public string TenantId { get; set; }

        public string Id { get; set; }
        public string CompanyId { get; set; }

        public bool EnableDashboard { get; set; }
        public string BrandedUniqueCode { get; set; }

        public string NmlsNumber { get; set; }
        public string BrandedSiteUrl { get; set; }
    }
}
