﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public class IndividualMemberResponse : IIndividualMemberResponse
    {
        public bool IsValid
        {
            get { return string.IsNullOrWhiteSpace(Message) ? true : false; }
        }

        public string Message { get; set; } = string.Empty;

        public IUserProfile Member { get; set; }
    }
}
