﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface IUserAccessModel
    {
        bool IsActive { get; set; }
        Dictionary<string, object> PasswordChangeOrExpiryOn { get; set; }
    }
}
