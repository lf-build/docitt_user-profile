﻿using System.IO;

namespace Docitt.UserProfile
{
    public class LogoDetails
    {
        public string FileName { get; set; }
        public Stream Content { get; set; }
    }
}