﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public class UserAccessModel : IUserAccessModel
    {
        public bool IsActive { get; set; }
        public Dictionary<string,object> PasswordChangeOrExpiryOn { get; set; }
    }
}
