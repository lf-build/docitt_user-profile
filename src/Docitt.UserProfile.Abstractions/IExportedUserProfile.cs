﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface IExportedUserProfile : IUserProfile
    {
        string LoanEventNotification { get; set; }
    }
}
