﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.UserProfile
{
    public interface IUserProfile : IAggregate
    {
        string UserId { get; set; }
        string Username { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string CompanyName { get; set; }
        string Title { get; set; }
        string License { get; set; }
        string MobileNumber { get; set; }
        string OfficePhone { get; set; }
        string Phone { get; set; }
        string Fax { get; set; }
        string Email { get; set; }
        string Address { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string LinkedInUrl { get; set; }
        string FacebookUrl { get; set; }
        string TwitterUrl { get; set; }
        string BranchName { get; set; }
        string Note { get; set; }
        string CompanyId { get; set; }
        List<UserRelationship> Relationships { get; set; }
        Dictionary<string, object> Settings { get; set; }
        PreferredCommunication Communication { get; set; }
        bool EnableDashboard { get; set; }
        string BrandedUniqueCode { get; set; }
        string NmlsNumber { get; set; }
        string BrandedSiteUrl { get; set; }
    }
}