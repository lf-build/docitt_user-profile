﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    /// <summary>
    /// This class is used only for Profile Configuration
    /// </summary>
    public class RoleNames
    {
        public string BackOffice { get; set; }
        public string Any { get; set; }
        public string LoanOfficer { get; set; }
        public string LoanOfficerAssistance { get; set; }
        public string Processor { get; set; }
        public string Underwriter { get; set; }
        public string BranchManager { get; set; }
        public string RegionalManager { get; set; }
        public string Admin { get; set; }
        public string Executive { get; set; }

        public string AffinityPartner { get; set; }
        public string AffinityPartnerAdmin { get; set; }
        public string AffinityPartnerManager { get; set; }
        public string AffinityPartnerAgent { get; set; }
        public string AffinityPartnerAssistant { get; set; }
        public string AffinityPartnerCoordinator { get; set; }
        public string AffinityPartnerOther { get; set; }
        public string Realtors { get; set; }
        public string Builders { get; set; }
        public string Accountants { get; set; }
        public string Brokers { get; set; }
        public string InsuranceAgents { get; set; }
        public string SellingAgent { get; set; }
        public string AssistantSellingAgent { get; set; }
        public string BuyingAgent { get; set; }
        public string AssistantBuyingAgent { get; set; }
        public string EscrowOfficer { get; set; }
        public string AssistantEscrowOfficer { get; set; }
        public string TitleAgent { get; set; }

        public string Borrower { get; set; }
        public string CoBorrower { get; set; }
    }
}    