﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public class LoanOfficer : ILoanOfficer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string BrandedSiteUrl { get; set; }
        public string BrandedUniqueCode { get; set; }
    }
}
