﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface IUserCompanyProfileRepository : IRepository<IUserCompanyProfile>
    {
        Task<IUserCompanyProfile> GetCompanyProfile(string companyId);
        Task<bool> UpdateCompanyProfileInfo(IUserCompanyProfile companyProfile, DateTimeOffset updatedDateTime);
    }
}