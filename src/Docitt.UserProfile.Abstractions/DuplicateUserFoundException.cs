﻿using System;

namespace Docitt.UserProfile
{
    public class DuplicateUserFoundException : Exception
    {
        public DuplicateUserFoundException(string username) : base(username)
        {

        }
        public DuplicateUserFoundException(string username, string message) : base(username + ":" + message)
        {
        }
    }
}
