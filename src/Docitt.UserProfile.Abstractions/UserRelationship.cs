﻿namespace Docitt.UserProfile
{
    public class UserRelationship
    {
        public string MemberName { get; set; }
        public string MemberRole { get; set; }
    }
}