namespace Docitt.UserProfile
{
    public class UserAutoSuggestion : IUserAutoSuggestion
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string BranchName { get; set; }
        public string State { get; set; }
    }
}