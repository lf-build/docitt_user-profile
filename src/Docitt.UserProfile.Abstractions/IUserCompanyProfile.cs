﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Docitt.UserProfile
{
    public interface IUserCompanyProfile : IAggregate
    {
        string CompanyName { get; set; }
        string Address { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string Phone { get; set; }
        string Website { get; set; }
        string LinkedInUrl { get; set; }
        string FacebookUrl { get; set; }
        string TwitterUrl { get; set; }
        string Note { get; set; }
        bool IsSuspended { get; set; }
        bool IsDeleted { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset UpdatedDate { get; set; }
    }
}