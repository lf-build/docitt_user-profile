﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface ILoanOfficer
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }
        string BrandedSiteUrl { get; set; }
        string BrandedUniqueCode { get; set; }
    }
}
