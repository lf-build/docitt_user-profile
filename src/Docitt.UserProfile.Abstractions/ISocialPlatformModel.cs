﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface ISocialPlatformModel
    {
        string Name { get; set; }
        string URL { get; set; }
    }
}
