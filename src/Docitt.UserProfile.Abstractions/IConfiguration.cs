﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace Docitt.UserProfile
{
    public interface IConfiguration : IDependencyConfiguration
    {
        List<EventMapping> EventMappings { get; set; }

        string[] AcceptedFileIn { get; set; }
        int MaxUploadFileSizeLimit { get; set; }

        List<UserSetting> Settings { get; set; }

        string BorrowerPortal { get; set; }
		RoleNames RoleNames { get; set; }

        List<string> RolesForBrandedSite { get; set; }
    }
}