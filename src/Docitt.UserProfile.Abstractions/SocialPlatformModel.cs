﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public class SocialPlatformModel : ISocialPlatformModel
    {
        public string Name { get; set; }
        public string URL { get; set; }
    }
}
