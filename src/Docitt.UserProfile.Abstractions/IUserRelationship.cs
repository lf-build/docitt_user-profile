﻿namespace Docitt.UserProfile
{
    public interface IUserRelationship
    {
        string MemberName { get; set; }
        string MemberRole { get; set; }
    }
}