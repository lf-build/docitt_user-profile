using System.Collections.Generic;

namespace Docitt.UserProfile
{
    public class Configuration : IConfiguration
    {
        public List<EventMapping> EventMappings { get; set; }
        public string[] AcceptedFileIn { get; set; }
        public int MaxUploadFileSizeLimit { get; set; }
        public List<UserSetting> Settings { get; set; }
        public string BorrowerPortal { get; set; }
        public string Database { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string ConnectionString { get; set; }
        public RoleNames RoleNames { get; set; }
        public List<string> RolesForBrandedSite { get; set; }
    }
}