﻿using System;

namespace Docitt.UserProfile
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "profile";
        public static string SignUpInvitedBy { get; } = "info@sigma.net";
    }
}
