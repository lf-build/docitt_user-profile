﻿namespace Docitt.UserProfile
{
    public class VerifyBrandedSiteTokenResponse: IVerifyBrandedSiteTokenResponse
    {
        public string BrandedSiteToken { get; set; }
        public bool IsValid { get; set; }
    }
}