﻿using Docitt.UserProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public interface IGroupMemberResponse
    {
        List<IUserProfile> SucceededMembers { get; set; }
        List<IUserProfile> InvalidMembers { get; set; }
        List<IUserProfile> DuplicatedMembers { get; set; }
    }
}
