﻿using LendFoundry.Security.Tokens;

namespace Docitt.UserProfile
{
    public interface IUserProfileRepositoryFactory
    {
        IUserProfileRepository Create(ITokenReader reader);
    }
}