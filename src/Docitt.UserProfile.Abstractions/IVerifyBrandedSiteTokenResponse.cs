﻿namespace Docitt.UserProfile
{
    public interface IVerifyBrandedSiteTokenResponse
    {
        string BrandedSiteToken { get; set; }
        bool IsValid { get; set; }
    }
}