﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Docitt.UserProfile
{
    public class UserSetting
    {
        public string Name { get; set; }
        public object DefaultValue { get; set; }
    }
}
