﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public enum SocialPlatforms
    {
        LinkedIn,
        Facebook,
        Twitter
    }
}
