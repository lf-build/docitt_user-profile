﻿namespace Docitt.UserProfile
{
    public class EventMapping
    {
        public string EventName { get; set; }
        public string Username { get; set; }
    }
}