﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;

namespace Docitt.UserProfile
{
    public class UserCompanyProfile : Aggregate, IUserCompanyProfile
    { 
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public string LinkedInUrl { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string Note { get; set; }
        public bool IsSuspended { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset UpdatedDate { get; set; }
    }
}