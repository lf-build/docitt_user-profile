namespace Docitt.UserProfile
{
    public interface IUserAutoSuggestion
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string UserName { get; set; }
        string BranchName { get; set; }
        string State { get; set; }
    }
}