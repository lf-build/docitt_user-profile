﻿namespace Docitt.UserProfile
{
    public class PreferredCommunication
    {
        public bool Text {get; set; }
        public bool Email { get; set; }
        public bool Phone { get; set; }
    }
}