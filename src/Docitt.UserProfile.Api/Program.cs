﻿#if DOTNET2

using System;
using Microsoft.AspNetCore.Hosting;

namespace Docitt.UserProfile.Api
{
    /// <summary>
    /// Program class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entrypoint
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine("Started on port 5000. Use Ctrl-C to shutdown the host...");
                host.WaitForShutdown();
            }
        }


    }
}

#endif