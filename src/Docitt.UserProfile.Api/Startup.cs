﻿using Docitt.UserProfile.Mongo;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using System;
using System.Runtime;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.ServiceDependencyResolver;

namespace Docitt.UserProfile.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittUserProfile"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
               {
                   Type = "apiKey",
                   Name = "Authorization",
                   Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                   In = "header"
               });
               c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.UserProfile.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddIdentityService();
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddLookupService();
            services.AddDecisionEngine();
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddDocumentManager();

            services.AddTransient<IHelperService, HelperService>();
            services.AddTransient<IUserProfileRepository, UserProfileRepository>();
            services.AddTransient<IUserCompanyProfileRepository, UserCompanyProfileRepository>();
            services.AddTransient<IUserProfileService, UserProfileService>();
            services.AddTransient<IUserCompanyProfileService, UserCompanyProfileService>();
            services.AddTransient<IUserProfileListener, UserProfileListener>();
            services.AddTransient<IUserProfileRepositoryFactory, UserProfileRepositoryFactory>();
            services.AddMongoConfiguration(Settings.ServiceName);

            services.AddTransient<IConfiguration>(p => p.GetService<IConfigurationService<Configuration>>().Get());
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT User Profile Service");
            });
     
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseUserProfileListener();
            app.UseMvc();  
            app.UseConfigurationCacheDependency();
        }
    }
}