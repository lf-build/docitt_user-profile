﻿using Docitt.UserProfile.Api.Utilities;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.IO;
using System.Threading.Tasks;

namespace Docitt.UserProfile.Api.CustomActions
{
    internal class FileResultFromStream : ActionResult
    {
        public FileResultFromStream(string fileDownloadName, Stream fileStream, string contentType = null)
        {
            FileDownloadName = fileDownloadName;
            FileStream = fileStream;
            ContentType = contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(fileDownloadName));
        }

        private string ContentType { get; }
        private string FileDownloadName { get; }
        private Stream FileStream { get; }

        public override async Task ExecuteResultAsync(ActionContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = ContentType;
            response.Headers.Add("Content-Disposition", new[] { "attachment; filename=" + FileDownloadName });
            await FileStream.CopyToAsync(context.HttpContext.Response.Body);
            FileStream.Dispose();
        }
    }
}