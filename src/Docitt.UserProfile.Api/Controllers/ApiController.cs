﻿using Docitt.UserProfile.Api.CustomActions;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Docitt.UserProfile.Api.Controllers
{
    /// <summary>
    /// Represents API controller class.
    /// </summary>
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="profileService"></param>
        /// <param name="companyProfileService"></param>
        public ApiController(
            IUserProfileService profileService, 
            IUserCompanyProfileService companyProfileService)
        {
            ProfileService = profileService;
            CompanyProfileService = companyProfileService;
        }

        private IUserProfileService ProfileService { get; }
        private IUserCompanyProfileService CompanyProfileService { get; }


        /// <summary>
        /// Get user profile data with specific username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet("/{username}")]
        [ProducesResponseType(typeof(IUserProfile),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetProfile(string username)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await ProfileService.GetProfile(username);
                return  Ok(res);
            });
        }

        /// <summary>
        /// Add new Company
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        [HttpPost("/company")]
        [ProducesResponseType(typeof(string ), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddCompanyProfile([FromBody] UserCompanyProfile profile)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await CompanyProfileService.AddCompanyProfile(profile);
                return Ok(res.Id);
            });
        }

        /// <summary>
        /// Update company profile info.
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        [HttpPut("/company")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateCompanyProfileInfo([FromBody] UserCompanyProfile profile)
        {
            return await ExecuteAsync(async () =>
            {
                /*return true if updated successfully*/
                var res = await CompanyProfileService.UpdateCompanyProfileInfo(profile);
                return Ok(res);
            });
        }


        /// <summary>
        /// Get company profile with specific company id.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet("/company/{companyId}")]
        [ProducesResponseType(typeof(IUserCompanyProfile), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> GetCompanyProfile(string companyId)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await CompanyProfileService.GetCompanyProfile(companyId,"company");
                return Ok(res);
            });
        }

        /// <summary>
        /// Enable user dashboard with specific username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>

        [HttpPut("/user/{userName}/enable-dashboard")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> EnableDashboard(string userName)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await ProfileService.EnableDashboard(userName);
                return Ok(res);
            });
        }

        /// <summary>
        /// Get company profile by user with specific username.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet("/company/user/{userName}")]
        [ProducesResponseType(typeof(IUserCompanyProfile), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetCompanyProfileByUser(string userName)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await CompanyProfileService.GetCompanyProfile(userName, "user");
                return Ok(res);
            });
        }

        /// <summary>
        /// Update user profile company info with specific company id.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpPost("/company/{companyId}/userprofile/update")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateUserProfileCompanyInfo(string companyId)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await CompanyProfileService.UpdateUserProfileCompanyInfo(companyId);
                return Ok(res);
            });
        }

        /// <summary>
        /// Add user profile.
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        [HttpPost("/")]
        [ProducesResponseType(typeof(IUserProfile), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Add([FromBody] UserProfile profile)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await ProfileService.AddProfile(profile);
                return Ok(res);
            });
        }

        
        /// <summary>
        /// Update user profile.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        [HttpPut("/{username}")]
        [ProducesResponseType(typeof(IUserProfile), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Update(string username, [FromBody] UserProfile profile)
        {
            return await ExecuteAsync(async () =>
            {
                 if(!string.IsNullOrEmpty(username))
                    username = username.ToLower();

                var res = await ProfileService.UpdateProfile(username, profile);
                return Ok(res);
            });
        }

        
        /// <summary>
        /// Upload logo woth specific entity type, entity id and file.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/upload-logo")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UploadLogo(string entityType, string entityId, IFormFile file)
        {
            MemoryStream ms = new MemoryStream();
            file.OpenReadStream().CopyTo(ms);
            ms.Position = 0;
            var fileName = ContentDispositionHeaderValue
            .Parse(file.ContentDisposition)
                                     .FileName
                                     .Trim('"');

            var objLogo = new LogoDetails() { FileName = fileName, Content = ms };

            return await ExecuteAsync(async () =>
            {
                await ProfileService.UploadLogo(entityType, entityId, objLogo);
                return NoContent();
            });
        }

        /// <summary>
        /// Get user profile logo with specific entity type and entity Id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/logo")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLogo(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                var logo = await ProfileService.GetLogo(entityType, entityId);
                if (logo.Content != null)
                {
                    return new FileResultFromStream(logo.FileName, logo.Content);
                }
                else
                {
                    return NoContent();
                }
            });
        }
        /// <summary>
        /// Update user relationship.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="relationships"></param>
        /// <returns></returns>
        [HttpPut("/{username}/relationships")]
        [ProducesResponseType(typeof(IEnumerable<UserRelationship>),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UserRelationship([FromRoute]string username, [FromBody] List<UserRelationship> relationships)
        {
            return await ExecuteAsync(async () => Ok(await ProfileService.UpdateUserRelationship(username, relationships)));
        }

        /// <summary>
        /// Get user relationships with specific username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet("/{username}/relationships/all")]
        [ProducesResponseType(typeof(IEnumerable<UserRelationship>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UserRelationships(string username)
        {
            return await ExecuteAsync(async () => Ok(await ProfileService.GetUserRelationships(username)));
        }

        /// <summary>
        ///  Update user access with specific username
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userProfileSettings"></param>
        /// <returns></returns>
        [HttpPut("/{username}/access")]
        [ProducesResponseType(typeof(UserAccessModel), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UserAccess([FromRoute]string username, [FromBody] UserAccessModel userProfileSettings)
        {
            return await ExecuteAsync(async () => Ok(await ProfileService.UpdateUserAccess(username, userProfileSettings)));
        }

        /// <summary>
        /// Get user access with specific username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet("/{username}/access")]
        [ProducesResponseType(typeof(UserAccessModel), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserAccess([FromRoute]string username)
        {
            return await ExecuteAsync(async () => Ok(await ProfileService.GetUserAccess(username)));
        }

        /// <summary>
        /// Add group members with specific entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPut("{entityType}/{entityId}/group-members/import")]
        [ProducesResponseType(typeof(IGroupMemberResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddGroupMembers(string entityType, string entityId, IFormFile file)
        {
            return await ExecuteAsync(async () =>
            {
                MemoryStream ms = new MemoryStream();
                file.OpenReadStream().CopyTo(ms);
                ms.Position = 0;
                var fileName = ContentDispositionHeaderValue
                    .Parse(file.ContentDisposition)
                    .FileName
                    .Trim('"');

                PostedFileDetails fileDetails = new PostedFileDetails()
                {
                    FileName = fileName,
                    Content = ms
                };

                return Ok(await ProfileService.AddGroupMembers(entityType, entityId, fileDetails));
            });
        }

       /// <summary>
       /// Add Individual member with specific entity type and entity id.
       /// </summary>
       /// <param name="entityType"></param>
       /// <param name="entityId"></param>
       /// <param name="profile"></param>
       /// <returns></returns>
        [HttpPut("{entityType}/{entityId}/individual-member")]
        [ProducesResponseType(typeof(IIndividualMemberResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddIndividualMember(string entityType, string entityId, [FromBody] UserProfile profile)
        {
            return await ExecuteAsync(async () => Ok(await ProfileService.AddIndividualMember(entityType, entityId, profile)));
        }

        /// <summary>
        /// Update social platform with specific username and social platforms.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="socialPlatforms"></param>
        /// <returns></returns>
        [HttpPut("{username}/social-platform")]
        [ProducesResponseType(typeof(ISocialPlatformModel), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateSocialPlatform(string username, [FromBody] SocialPlatformModel socialPlatforms)
        {
            return await ExecuteAsync(async () => Ok(await ProfileService.UpdateSocialPlatform(username, socialPlatforms)));
        }

        /// <summary>
        /// Update settings with specific username,key and value.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPut("{username}/setting/{key}/update")]
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateSettings(string username, string key, [FromBody] object value)
        {
            return await ExecuteAsync(async () => Ok(await ProfileService.UpdateSettings(username, key, value)));
        }

        /// <summary>
        /// Get setting data with username and key.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet("{username}/setting/{key}")]
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetSetting(string username, string key)
        {
            return await ExecuteAsync(async () => Ok(await ProfileService.GetSetting(username, key)));
        }

        /// <summary>
        /// Get all settings with username.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet("{username}/setting/all")]
        [ProducesResponseType(typeof(Dictionary<string,object>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllSettings(string username)
        {
            return await ExecuteAsync(async () => Ok(await ProfileService.GetAllSettings(username)));
        }

        /// <summary>
        /// GetLoanOfficer
        /// </summary>
        /// <param name="loanOfficerEmail"></param>
        /// <returns></returns>
        [HttpGet("/user/loanofficer/{loanOfficerEmail}/brandedsiteurl")]
        [ProducesResponseType(typeof(IUserProfile),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoanOfficer(string loanOfficerEmail)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await ProfileService.GetLoanOfficer(loanOfficerEmail);
                return Ok(res);
            });
        }

        /// <summary>
        /// GetProfileByUniqueCode
        /// </summary>
        /// <param name="brandedUniqueCode"></param>
        /// <returns></returns>
        [HttpGet("/user/loanofficer/{brandedUniqueCode}")]
        [ProducesResponseType(typeof(IUserProfile),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetProfileByUniqueCode(string brandedUniqueCode)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await ProfileService.GetProfileByUniqueCode(brandedUniqueCode);
                return Ok(res);
            });
        }
        
        /// <summary>
        /// VerifyBrandedSiteToken
        /// </summary>
        /// <param name="brandedSiteToken">request</param>
        /// <returns></returns>
        [HttpGet("/user/loanofficer/{brandedSiteToken}/verifybrandedsitetoken")]
        [ProducesResponseType(typeof(IVerifyBrandedSiteTokenResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> VerifyBrandedSiteToken(string brandedSiteToken)
        {
            return await ExecuteAsync(async () =>
            {
                var response = await ProfileService.VerifyBrandedSiteToken(brandedSiteToken);
                return Ok(response);
            });
        }

        /// <summary>
        /// GetProfile
        /// </summary>
        /// <param name="fname">firstname</param>
        /// <param name="lname">lastname</param>
        /// <param name="email">email</param>
        /// <returns></returns>
        [HttpGet("/user/fname/{fname}/lname/{lname}/email/{email}")]
        [ProducesResponseType(typeof(IUserProfile), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetProfile(string fname, string lname, string email)
        {
            return await ExecuteAsync(async () =>
            {
                var response = await ProfileService.GetProfile(fname, lname, email);
                return Ok(response);
            });
        }

        /// <summary>
        /// GetProfileByBrandedSiteCode
        /// </summary>
        /// <param name="brandedUniqueCode"></param>
        /// <returns></returns>
        [HttpGet("/user/brandedcode/{brandedUniqueCode}")]
        [ProducesResponseType(typeof(IUserProfile), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetProfileByBrandedSiteCode(string brandedUniqueCode)
        {
            return await ExecuteAsync(async () =>
            {
                var res = await ProfileService.GetProfileByUniqueCode(brandedUniqueCode);
                return Ok(res);
            });
        }
        
        /// <summary>
        /// Get LoanOfficer's List
        /// </summary>
        /// <returns></returns>
        [HttpGet("/users/lo")]
        [ProducesResponseType(typeof(List<IUserAutoSuggestion>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoanOfficerList()
        {
            return await ExecuteAsync(async () =>
            {
                var res = await ProfileService.GetLoanOfficerList();
                return Ok(res);
            });
        }
    }
}