﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Threading.Tasks;

namespace Docitt.UserProfile.Mongo
{
    public class UserCompanyProfileRepository : MongoRepository<IUserCompanyProfile, UserCompanyProfile>, IUserCompanyProfileRepository
    {
        static UserCompanyProfileRepository()
        {
            BsonClassMap.RegisterClassMap<UserCompanyProfile>(map =>
            {
                map.AutoMap();
                var type = typeof(UserCompanyProfile);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public UserCompanyProfileRepository(ITenantService tenantService, IMongoConfiguration configuration) :
            base(tenantService, configuration, "companyprofile")
        {
            
        }

        public async Task<IUserCompanyProfile> GetCompanyProfile(string companyId)
        {
            return await Query.FirstOrDefaultAsync(r => r.Id == companyId);
        }

        public async Task<bool> UpdateCompanyProfileInfo(IUserCompanyProfile companyProfile, DateTimeOffset updatedDateTime)
        {
            await Collection.UpdateOneAsync
              (
                  Builders<IUserCompanyProfile>.Filter
                      .Where(a => a.TenantId == TenantService.Current.Id && a.Id == companyProfile.Id),
                  Builders<IUserCompanyProfile>.Update
                        .Set(a => a.CompanyName, companyProfile.CompanyName)
                        .Set(a => a.Address, companyProfile.Address)
                        .Set(a => a.Phone, companyProfile.Phone)
                        .Set(a => a.City, companyProfile.City)
                        .Set(a => a.State, companyProfile.State)
                        .Set(a => a.ZipCode, companyProfile.ZipCode)
                        .Set(a => a.LinkedInUrl, companyProfile.LinkedInUrl)
                        .Set(a => a.TwitterUrl, companyProfile.TwitterUrl)
                        .Set(a => a.FacebookUrl, companyProfile.FacebookUrl)
                        .Set(a => a.UpdatedDate, updatedDateTime)
                        .Set(a => a.Website ,companyProfile.Website) 
            );
            return true;
        }
    }
}