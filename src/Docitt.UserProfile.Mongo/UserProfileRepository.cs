﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.UserProfile.Mongo
{
    public class UserProfileRepository : MongoRepository<IUserProfile, UserProfile>, IUserProfileRepository
    {
        static UserProfileRepository()
        {
            BsonClassMap.RegisterClassMap<UserProfile>(map =>
            {
                map.AutoMap();
                //map.MapMember(f => f.Communication).SetSerializer(new EnumSerializer<PreferredCommunication>(BsonType.String));
                var type = typeof(UserProfile);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.UnmapMember(i => i.BrandedSiteUrl);
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<PreferredCommunication>(map =>            
            {
                map.AutoMap();
                var type = typeof(PreferredCommunication);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public UserProfileRepository(ITenantService tenantService, IMongoConfiguration configuration) :
            base(tenantService, configuration, "profile")
        {
            CreateIndexIfNotExists("entity-type", Builders<IUserProfile>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Username),true);
            CreateIndexIfNotExists("branded-unique-code", Builders<IUserProfile>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.BrandedUniqueCode));
            CreateIndexIfNotExists("firstname-lastname-email", Builders<IUserProfile>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.FirstName).Ascending(i => i.LastName).Ascending(i => i.Email));
            CreateIndexIfNotExists("title", Builders<IUserProfile>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Title));
        }

        public async Task<IUserProfile> GetByUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await Query.FirstOrDefaultAsync(r => r.Username == username);
            return user;
        }

        public async Task<IUserProfile> GetByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentNullException(nameof(email));

            var user = await Query.FirstOrDefaultAsync(r => r.Email == email);
            return user;
        }

        public async Task<IUserProfile> GetProfile(string firstname, string lastname, string email)
        {
            if (string.IsNullOrEmpty(firstname))
                throw new ArgumentNullException(nameof(firstname));

            if (string.IsNullOrEmpty(lastname))
                throw new ArgumentNullException(nameof(lastname));

            if (string.IsNullOrEmpty(email))
                throw new ArgumentNullException(nameof(email));

            var user = await Query.FirstOrDefaultAsync(r => r.FirstName == firstname.Trim() && r.LastName == lastname.Trim() && 
                                                       r.Email == email.Trim().ToLower());
            return user;
        }

        public async Task<IUserProfile> GetById(string uid)
        {
            if (string.IsNullOrEmpty(uid))
                throw new ArgumentNullException(nameof(uid));

            var user = await Query.FirstOrDefaultAsync(r => r.Id == uid);
            return user;
        }

        public async Task<IUserProfile> UpdateProfile(string username, IUserProfile userProfile)
        {
            await Collection.UpdateOneAsync
              (
                  Builders<IUserProfile>.Filter
                      .Where(a => a.TenantId == TenantService.Current.Id &&
                                  a.Username == username),
                  Builders<IUserProfile>.Update
                        .Set(a => a.FirstName, userProfile.FirstName)
                        .Set(a => a.LastName, userProfile.LastName)
                        .Set(a => a.CompanyName, userProfile.CompanyName)
                        .Set(a => a.Title, userProfile.Title) 
                        .Set(a => a.License, userProfile.License)
                        .Set(a => a.MobileNumber, userProfile.MobileNumber)
                        .Set(a => a.OfficePhone, userProfile.OfficePhone)
                        .Set(a => a.Phone, userProfile.Phone)
                        .Set(a => a.Fax, userProfile.Fax)
                        .Set(a => a.Email, userProfile.Email)
                        .Set(a => a.Address, userProfile.Address)
                        .Set(a => a.City, userProfile.City)
                        .Set(a => a.State, userProfile.State)
                        .Set(a => a.ZipCode, userProfile.ZipCode)
                        .Set(a => a.LinkedInUrl, userProfile.LinkedInUrl)
                        .Set(a => a.TwitterUrl, userProfile.TwitterUrl)
                        .Set(a => a.FacebookUrl, userProfile.FacebookUrl)
                        .Set(a => a.BranchName, userProfile.BranchName)
                        .Set(a => a.Note, userProfile.Note)
                        .Set(a => a.Communication, userProfile.Communication)
                        .Set(a => a.NmlsNumber, userProfile.NmlsNumber)
                      /*note: not to set companyId mapping here*/
                      /*note: not to set branded unqiue code here*/
                      );
            return userProfile;
        }

        public async Task<bool> UpdateUserProfileCompanyInfo(string username, string companyId)
        {
            await Collection.UpdateOneAsync
              (
                  /*note: set companyId mapping here*/
                  Builders<IUserProfile>.Filter
                      .Where(a => a.TenantId == TenantService.Current.Id &&
                                  a.Username == username),
                  Builders<IUserProfile>.Update.Set(a => a.CompanyId, companyId)
              );
            return true;
        }

        public async Task<bool> UpdateEnableDashboard(string userName)
        {
            await Collection.UpdateOneAsync
              (
                 /* need to update the user completed signup profile flow */
                  Builders<IUserProfile>.Filter
                      .Where(a => a.TenantId == TenantService.Current.Id &&
                                  a.Username == userName),
                  Builders<IUserProfile>.Update.Set(a => a.EnableDashboard, true)
              );
            return true;
        }

        public async Task<IUserProfile> GetByUniqueCode(string brandedUniqueCode)
        {
            if (string.IsNullOrEmpty(brandedUniqueCode))
                throw new ArgumentNullException(nameof(brandedUniqueCode));
            
            var user = await Query.FirstOrDefaultAsync(r => r.BrandedUniqueCode == brandedUniqueCode);
            return user;
        }
        
        public async Task<string> GetBrandedUniqueCode()
        {
            var brandedUniqueCode = $"{Guid.NewGuid():N}";
            var isExists = await Query.FirstOrDefaultAsync(r => r.BrandedUniqueCode == brandedUniqueCode);
            if(isExists != null)
            {
                await GetBrandedUniqueCode();
            }
            return brandedUniqueCode;
        }

        public async Task<List<IUserAutoSuggestion>> GetUserListByTitle(string title)
        {
            var users = await Query.Where
                                (r => r.Title == title)
                                .Select(
                                    u => new UserAutoSuggestion()
                                    {
                                        FirstName = u.FirstName,
                                        LastName = u.LastName,
                                        UserName = u.Username,
                                        BranchName = u.BranchName,
                                        State = u.State
                                    }
                                ).ToListAsync();
            return users.Cast<IUserAutoSuggestion>().ToList();
        }
    }
}