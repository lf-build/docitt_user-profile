﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;
using LendFoundry.Foundation.Logging;

namespace Docitt.UserProfile.Mongo
{
    public class UserProfileRepositoryFactory : IUserProfileRepositoryFactory
    {
        public UserProfileRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IUserProfileRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new UserProfileRepository(tenantService, mongoConfiguration);
        }
    }
}