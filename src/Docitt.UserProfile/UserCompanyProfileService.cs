﻿using Docitt.UserProfile.Events;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using System;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public class UserCompanyProfileService : IUserCompanyProfileService
    {
        public UserCompanyProfileService(
            ILogger logger,
            IUserCompanyProfileRepository companyRepository,
            IUserProfileRepository userRepository,
            IHelperService helperService,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime)
        {
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");

            Logger = logger;
            CompanyRepository = companyRepository;
            UserRepository = userRepository;
            Helper = helperService;
            EventHubClient = eventHubClient;
            TenantTime = tenantTime;
        }

        
        private ILogger Logger { get; }
        private IUserCompanyProfileRepository CompanyRepository { get; }
        private IUserProfileRepository UserRepository { get; }
        private IHelperService Helper { get; }
        private IEventHubClient EventHubClient { get; }
        private ITenantTime TenantTime { get; }

        public async Task<IUserCompanyProfile> GetCompanyProfile(string val,string searchKey)
        {
            if (string.IsNullOrEmpty(val))
                throw new ArgumentNullException(nameof(val));

            string companyId = string.Empty;

            if (searchKey == "user")
            {
                var user = await UserRepository.GetByUsername(val);
                companyId = user.CompanyId; /*get company id*/
            }
            else
                companyId = val;

            if (string.IsNullOrEmpty(companyId))
                throw new ArgumentNullException("Company Profile not found");

            return await CompanyRepository.GetCompanyProfile(companyId);
        }

        public async Task<IUserCompanyProfile> AddCompanyProfile(IUserCompanyProfile profile)
        {
            if (profile == null)
                throw new ArgumentNullException(nameof(profile));

            return await Task.Run(() => {

                profile.CreatedDate = TenantTime.Now;
                profile.UpdatedDate = TenantTime.Now; 
                // Add entry 
                CompanyRepository.Add(profile);
                return profile;
            });
        }

        public async Task<bool> UpdateCompanyProfileInfo(IUserCompanyProfile companyProfile)
        {
            if (companyProfile == null)
                throw new ArgumentNullException(nameof(companyProfile));

            if (companyProfile.Id == null)
                throw new ArgumentNullException(nameof(companyProfile.Id));

            /*Update Company Profile*/
            return await CompanyRepository.UpdateCompanyProfileInfo(companyProfile, TenantTime.Now);
        }

        public async Task<bool> UpdateUserProfileCompanyInfo(string companyId)
        {
            var userName = Helper.GetCurrentUser();
            var result = await UserRepository.UpdateUserProfileCompanyInfo(userName,companyId);
            // Raise event for UserProfileModified
            var profile = UserRepository.GetByUsername(userName).Result;
            await EventHubClient.Publish(new UserProfileModified() { UserProfile = profile });
            return result;
        }
    }
}