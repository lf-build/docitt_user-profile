﻿using LendFoundry.Security.Tokens;
using System;

namespace Docitt.UserProfile
{
    public interface IHelperService
    {
        string GetCurrentUser();
    }

    public class HelperService : IHelperService
    {
        public HelperService(ITokenReader tokenReader, ITokenHandler tokenParser)
        {
            if (tokenReader == null)
                throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenParser == null)
                throw new ArgumentException($"{nameof(tokenParser)} is mandatory");

            TokenReader = tokenReader;
            TokenParser = tokenParser;
        }

        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }

        public string GetCurrentUser()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("username is mandatory");
            return username;
        }
    }
}
