﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.UserProfile
{
    public static class StringExtension
    {
        public static string DocittUrlDecode(this String str)
        {
            var decodeUrl = WebUtility.UrlDecode(str);
            return decodeUrl.Replace(" ", "+");
        }
    }
}
