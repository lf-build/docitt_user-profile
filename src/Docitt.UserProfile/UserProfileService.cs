﻿
using CsvHelper;
using Docitt.UserProfile.Events;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using LendFoundry.Security.Identity.Client;
using AutoMapper;

namespace Docitt.UserProfile
{
    public class UserProfileService : IUserProfileService
    {
        public UserProfileService(IConfiguration configuration,
            IEventHubClient eventHubClient,
            IUserProfileRepository userProfileRepository,
            IDocumentManagerService documentManager,
            IIdentityService identityService,
            ILogger logger,
            ILookupService lookup,
            IDecisionEngineService decisionEngineFactory
            )
        {
            if (lookup == null) throw new ArgumentException($"{nameof(lookup)} is mandatory");
            Configuration = configuration;
            EventHubClient = eventHubClient;
            UserProfileRepository = userProfileRepository;
            DocumentManager = documentManager;
            IdentityService = identityService;
            Logger = logger;
            Lookup = lookup;
            DecisionEngineFactory = decisionEngineFactory;
        }

        private IConfiguration Configuration { get; }
        private IEventHubClient EventHubClient { get; }
        private IUserProfileRepository UserProfileRepository { get; }
        private IDocumentManagerService DocumentManager { get; }
        private IIdentityService IdentityService { get; }
        private IDecisionEngineService DecisionEngineFactory;
        private ILogger Logger { get; }
        private ILookupService Lookup { get; }
        private const int NmlsNumberLength = 8;
        
        /// <summary>
        /// Get the user profile
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<IUserProfile> GetProfile(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(nameof(username));
            
            username = username.Trim().ToLower().DocittUrlDecode();            

            IUserProfile user = new UserProfile();
            user = UserProfileRepository.GetByUsername(username).Result;
            if (user == null)
            {
                Logger.Debug($"User {username} cannot be found. It will try max 4 times after each 2 second delay");
                throw new NotFoundException($"User {username} cannot be found");     
            }
            user.BrandedSiteUrl = $"{Configuration.BorrowerPortal}{user.BrandedUniqueCode}";
            return await Task.Run(() => user);
        }

        public async Task<ILoanOfficer> GetLoanOfficer(string email)
        {
            Logger.Debug("Inside GetLoanOfficer...");
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email));

            email = email.Trim().DocittUrlDecode();

            var user = await UserProfileRepository.GetByUsername(email);

            if (user == null)
            {
                Logger.Error($"User Profile with {email} cannot be found");
                throw new NotFoundException($"User Profile with {email} cannot be found");
            }

            /* Commenting as ,insted of that checking with brandedUniqueCode is available for user
                branded unique code is only for LO & AP.         
            
            if (user.Title == null || user.Title.ToLower() != Configuration.RoleNames.LoanOfficer.ToLower())
            {
                Logger.Error($"{email} with Title 'Loan Officer' cannot be found");
                throw new NotFoundException($"{email} with Title 'Loan Officer' cannot be found");
            }
             */
            // branded unique code is only for LO & AP

             if(string.IsNullOrEmpty(user.BrandedUniqueCode))
             {
                  Logger.Error($"Branded UniqueCode is not found for {email}");
                throw new NotFoundException($"Branded UniqueCode is not found for {email}");
             }

            ILoanOfficer loanOfficer = new LoanOfficer();
            loanOfficer.FirstName = user.FirstName;
            loanOfficer.LastName = user.LastName;
            loanOfficer.Email = user.Email;
            loanOfficer.BrandedUniqueCode = user.BrandedUniqueCode;

            loanOfficer.BrandedSiteUrl = $"{Configuration.BorrowerPortal}{user.BrandedUniqueCode}";
            return loanOfficer;           
        }

        public async Task<ILoanOfficerUniqueCodeResponse> GetProfileByUniqueCode(string brandedUniqueCode)
        {
            Logger.Debug("Inside GetProfileByUniqueCode...");
            if (string.IsNullOrWhiteSpace(brandedUniqueCode))
                throw new ArgumentNullException(nameof(brandedUniqueCode));

            brandedUniqueCode = brandedUniqueCode.Trim().DocittUrlDecode();

            var user = await UserProfileRepository.GetByUniqueCode(brandedUniqueCode);

            if (user == null)
            {
                Logger.Error($"BrandedUniqueCode {brandedUniqueCode} cannot be found");
                throw new NotFoundException($"BrandedUniqueCode {brandedUniqueCode} cannot be found");
            }
            //No need to check as brandedUniquecode is for LO & AP
            /*
            if (user.Title == null || !Configuration.RolesForBrandedSite.Contains(user.Title))
            {
                Logger.Error($"{brandedUniqueCode} with Title {user.Title} cannot be found");
                throw new NotFoundException($"{brandedUniqueCode} with Title '{user.Title}' cannot be found");
            }
            */

            ILoanOfficerUniqueCodeResponse loanOfficer = new LoanOfficerUniqueCodeResponse();
            loanOfficer.Username = user.Username;
            loanOfficer.FirstName = user.FirstName;
            loanOfficer.LastName = user.LastName;
            loanOfficer.Email = user.Email;
            loanOfficer.BrandedUniqueCode = user.BrandedUniqueCode;
            loanOfficer.NmlsNumber = user.NmlsNumber;
            loanOfficer.Phone = user.Phone;
            loanOfficer.MobileNumber = user.MobileNumber;
            loanOfficer.Title = user.Title;
            return loanOfficer;
        }

        public async Task<IVerifyBrandedSiteTokenResponse> VerifyBrandedSiteToken(string brandedUniqueCode)
        {
            Logger.Debug("Inside VerifyBrandedSiteToken...");
            if (string.IsNullOrWhiteSpace(brandedUniqueCode))
                throw new ArgumentNullException(nameof(brandedUniqueCode));

            IVerifyBrandedSiteTokenResponse verifyBrandedSiteTokenResponse = new VerifyBrandedSiteTokenResponse();
            verifyBrandedSiteTokenResponse.BrandedSiteToken = brandedUniqueCode;
            verifyBrandedSiteTokenResponse.IsValid = true;
            
            var user = await UserProfileRepository.GetByUniqueCode(brandedUniqueCode);
            if (user == null)
            {
                verifyBrandedSiteTokenResponse.IsValid = false;
            }
           
            return verifyBrandedSiteTokenResponse;           
        }
        
        public async Task<IUserProfile> AddProfile(IUserProfile profile)
        {
            EnsureDataIsValid(profile);

            if (profile.Settings == null)
            {
                profile.Settings = new Dictionary<string, object>();
                var usersetting = Configuration.Settings;

                foreach (var setting in usersetting)
                {
                    profile.Settings.Add(setting.Name, JsonConvert.SerializeObject(setting.DefaultValue));
                }
            }

            var userName = profile.Username.DocittUrlDecode();
            EnsureNoDuplicatedUser(userName);

            if(profile != null && !string.IsNullOrEmpty(profile.Email))
            {
                profile.Email = profile.Email.Trim().ToLower();
            }

            if(profile != null && !string.IsNullOrEmpty(profile.FirstName))
            {
                profile.FirstName = profile.FirstName.Trim().ToString();
            }

            if(profile != null && !string.IsNullOrEmpty(profile.LastName))
            {
                profile.LastName = profile.LastName.Trim().ToString();
            }
            
            UserProfileRepository.Add(profile);
            await EventHubClient.Publish(new UserProfileCreated() { UserProfile = profile });
            return profile;
        }

        public async Task<IUserProfile> UpdateProfile(string username, IUserProfile profile)
        {
            if (profile == null)
                throw new ArgumentNullException(nameof(profile));
            profile.Username = username;
            EnsureDataIsValid(profile);
            username = username.DocittUrlDecode();

            var userProfile = GetUserProfileByUserName(username);

            if (userProfile == null)
                throw new NotFoundException($"User {username} cannot be found");

            if(profile != null && !string.IsNullOrEmpty(profile.Email))
            {
                profile.Email = profile.Email.Trim().ToLower();
            }

            if(profile != null && !string.IsNullOrEmpty(profile.FirstName))
            {
                profile.FirstName = profile.FirstName.Trim().ToString();
            }

            if(profile != null && !string.IsNullOrEmpty(profile.LastName))
            {
                profile.LastName = profile.LastName.Trim().ToString();
            }

            if (profile.Title != null && profile.Title.ToLower() == Configuration.RoleNames.LoanOfficer.ToLower())
            {
                if (profile.NmlsNumber != null)
                {
                    CheckNmlsNumberLength(profile);
                }
                else
                {
                    throw new InvalidArgumentException($"{nameof(profile.NmlsNumber)} is mandatory");
                }
            }
            else
            {
                //Nmls Number will be null, in case if role is not borrower
                profile.NmlsNumber = null;
            }
            //TODO Assign existing values to model. Do not store null values from payload
            userProfile = AssignUserProfile(userProfile, profile);
            
            await UserProfileRepository.UpdateProfile(username, userProfile);
            await EventHubClient.Publish(new UserProfileModified() { UserProfile = userProfile });
            return userProfile;
        }

        public async Task<IDocument> UploadLogo(string entityType, string entityId, LogoDetails logoDetail)
        {
            List<string> tags = new List<string>();
            tags.Add(entityType);

            var uploadedDocument = await DocumentManager.Create
                   (
                       logoDetail.Content,
                       entityType,
                       entityId,
                       logoDetail.FileName,
                       tags
                   );

            return uploadedDocument;
        }

        public async Task<LogoDetails> GetLogo(string entityType, string entityId)
        {
            var uploadedDocument = await DocumentManager.GetAllLatest(entityType, entityId);
            LogoDetails logoDetail = new LogoDetails();
            if (uploadedDocument != null)
            {
                var documentList = uploadedDocument.ToList();
                if (documentList.Count > 0)
                {
                    var documentStream = await DocumentManager.Download(entityType, entityId, documentList[0].Id);
                    logoDetail.FileName = documentList[0].FileName;
                    logoDetail.Content = documentStream;
                }
            }
            return logoDetail;
        }

        private void EnsureDataIsValid(IUserProfile profile)
        {
            if (profile == null)
                throw new InvalidArgumentException("user profile cannot be empty");
            if (string.IsNullOrWhiteSpace(profile.Username))
                throw new InvalidArgumentException($"{nameof(profile.Username)} is mandatory");
            // if (string.IsNullOrWhiteSpace(profile.FirstName))
            //     throw new InvalidArgumentException($"{nameof(profile.FirstName)} is mandatory");
            // if (string.IsNullOrWhiteSpace(profile.LastName))
            //     throw new InvalidArgumentException($"{nameof(profile.LastName)} is mandatory");
            if (string.IsNullOrWhiteSpace(profile.Title))
                throw new InvalidArgumentException($"{nameof(profile.Title)} is mandatory");

            if (string.IsNullOrWhiteSpace(profile.Email))
                throw new InvalidArgumentException($"{nameof(profile.Email)} is mandatory");


            bool isValidEmail = Regex.IsMatch(profile.Email,
            @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
            RegexOptions.IgnoreCase | RegexOptions.Compiled);

            if (!isValidEmail)
                throw new InvalidArgumentException($"{nameof(profile.Email)} is invalid");

            if (!string.IsNullOrWhiteSpace(profile.Phone))
            {
                if (!Regex.IsMatch(profile.Phone, @"^\d{10}$"))
                    throw new InvalidArgumentException($"{nameof(profile.Phone)} is invalid");
            }

            if (!string.IsNullOrWhiteSpace(profile.ZipCode))
            {
                if (!Regex.IsMatch(profile.ZipCode, @"^[0-9]{5}$"))
                    throw new InvalidArgumentException($"{nameof(profile.ZipCode)} is invalid");
            }
        }

        private void EnsureNoDuplicatedUser(string username)
        {
            var userProfile = UserProfileRepository.GetByUsername(username).Result;

            if (userProfile != null)
                throw new DuplicateUserFoundException($"There is duplicated user in the payload, please verify with User with {username} already exists.");
        }

        private void CheckNmlsNumberLength(IUserProfile profile)
        {
            if (profile.NmlsNumber.Length > NmlsNumberLength)
                throw new InvalidArgumentException($"{nameof(profile.NmlsNumber)} must not be greater than 8 digits");
        }
        
        private IUserProfile GetUserProfileByUserName(string username)
        {
            return UserProfileRepository.GetByUsername(username).Result;
        }

        public async Task<IEnumerable<UserRelationship>> UpdateUserRelationship(string username, List<UserRelationship> relationships)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await GetProfile(username);
            if (user == null)
                throw new UserNotFoundException(username);

            if (relationships == null || !relationships.Any())
                throw new ArgumentNullException(nameof(relationships));

            user.Relationships = relationships;
            UserProfileRepository.Update(user);

            return user.Relationships;
        }

        public async Task<IEnumerable<UserRelationship>> GetUserRelationships(string username)
        {
            var user = await GetProfile(username);
            if (user == null)
                throw new UserNotFoundException(username);
            return user.Relationships;
        }

        public async Task<bool> EnableDashboard(string userName)
        {
            Logger.Info("Inside EnableDashboard...");
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));

            return await UserProfileRepository.UpdateEnableDashboard(userName);

        }

        public async Task<UserAccessModel> UpdateUserAccess(string username, UserAccessModel userAccessRequest)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await GetProfile(username);
            if (user == null)
                throw new NotFoundException($"User {username} cannot be found");

            if (userAccessRequest == null)
                throw new ArgumentNullException(nameof(userAccessRequest));

            if (userAccessRequest.PasswordChangeOrExpiryOn == null || !userAccessRequest.PasswordChangeOrExpiryOn.Any())
                throw new ArgumentNullException($"{nameof(userAccessRequest.PasswordChangeOrExpiryOn)} not found");

            var passwordChangeOrExpiryOnSetting = JsonConvert.SerializeObject(userAccessRequest.PasswordChangeOrExpiryOn, Formatting.Indented);

            user.Settings = AddOrUpdateUserSetting(user.Settings, nameof(userAccessRequest.PasswordChangeOrExpiryOn), passwordChangeOrExpiryOnSetting);

            UserProfileRepository.Update(user);

            try
            {
                if (userAccessRequest.IsActive)
                {
                    await IdentityService.ActivateUser(username);
                }
                else
                {
                    await IdentityService.DeActivateUser(username);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return userAccessRequest;
        }

        private Dictionary<string, object> AddOrUpdateUserSetting(Dictionary<string, object> profileSettings, string key, dynamic value)
        {
            if (profileSettings == null)
            {
                profileSettings = new Dictionary<string, object>();
                var usersetting = Configuration.Settings;

                foreach (var setting in usersetting)
                {
                    profileSettings.Add(setting.Name, JsonConvert.SerializeObject(setting.DefaultValue));
                }
            }

            var data = profileSettings.FirstOrDefault(x => string.Equals(x.Key, key, StringComparison.OrdinalIgnoreCase));
            if (data.Key == null)
                throw new ArgumentNullException("setting not found for key name: " + key);

            profileSettings[data.Key] = value;

            return profileSettings;
        }

        public async Task<UserAccessModel> GetUserAccess(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await GetProfile(username);
            if (user == null)
                throw new NotFoundException($"User {username} cannot be found");

            var settings = user.Settings;

            if (settings == null || !settings.Any() || !settings.ContainsKey("PasswordChangeOrExpiryOn"))
            {
                throw new NotFoundException($"User Access settings cannot be found");
            }

            object jsonObject = settings["PasswordChangeOrExpiryOn"];

            Dictionary<string, object> passwordChangeOrExpiryOn = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonObject.ToString());

            var userIdentity = await IdentityService.GetUser(username);
            if (userIdentity == null)
                throw new NotFoundException($"User {username} cannot be found");

            UserAccessModel responseModel = new UserAccessModel()
            {
                IsActive = userIdentity.IsActive,
                PasswordChangeOrExpiryOn = passwordChangeOrExpiryOn
            };

            return responseModel;
        }

        public async Task<IGroupMemberResponse> AddGroupMembers(string entityType, string entityId, PostedFileDetails file)
        {
            EnsureFileTypeIsValid(file);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            entityType = EnsureEntityTypeIsValid(entityType);

            var membersList = ReadCSVFile(file.Content);

            IGroupMemberResponse response = new GroupMemberResponse();

            foreach (var member in membersList)
            {
                IUserProfile memberUser = null;

                try
                {
                    if (member.LoanEventNotification != null)
                    {
                        int notification = Convert.ToInt32(member.LoanEventNotification);

                        if (notification > 0 && Enum.IsDefined(typeof(LoanEventNotification), notification))
                        {
                            Dictionary<string, object> loanEventNotificationObject = new Dictionary<string, object>()
                                                                                 { { ((LoanEventNotification)notification).ToString(), true } };

                            var loanEventNotification = JsonConvert.SerializeObject(loanEventNotificationObject);

                            member.Settings = AddOrUpdateUserSetting(member.Settings, nameof(LoanEventNotification), loanEventNotification);
                        }
                        else
                        {
                            throw new InvalidArgumentException($"{nameof(member.LoanEventNotification)} is invalid");
                        }
                    }
                    
                    Mapper.Initialize(cfg => {
                        cfg.CreateMap<IUserProfile, UserProfile>();                       
                    });

                    memberUser = Mapper.Map<UserProfile>(member);

                    await ImportUser(entityType, entityId, memberUser);

                    response.SucceededMembers.Add(memberUser);
                }
                catch (DuplicateUserFoundException)
                {
                    response.DuplicatedMembers.Add(memberUser);
                }
                catch (InvalidArgumentException)
                {
                    response.InvalidMembers.Add(memberUser);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return response;
        }

        private async Task<IUserProfile> ImportUser(string entityType, string entityId, IUserProfile profile)
        {
            profile.Username = profile.Email;
            if (!string.IsNullOrWhiteSpace(profile.Phone))
                profile.Phone = Regex.Replace(profile.Phone, @"(\s+|@|&|\.|'|-|\(|\)|<|>|\[|\]|\{|\}|#)", "");

            AddBranchIfNotExist(profile.BranchName);
           
            var result = await AddProfile(profile);

            if (result != null)
            {
                SendInvitation(entityType, entityId, profile);
            }

            return result;
        }

        private bool SendInvitation(string entityType, string entityId, IUserProfile profile)
        {
            try
            {
                var inviteMember = new
                {
                    InvitedBy = Settings.SignUpInvitedBy,
                    InviteEmail = profile.Email,
                    InviteFristName = profile.FirstName,
                    InviteLastName = profile.LastName,
                    Role = profile.Title,
                    Team = Team.lender.ToString()
                };

                var result = DecisionEngineFactory.Execute<dynamic, object>("dtmSendInvite", new
                {
                    entityType = entityType,
                    entityId = entityId,
                    payload = inviteMember
                });

                if (result != null)
                {
                    dynamic dResultStatus = JObject.Parse(result.ToString())["status"].ToString();

                    if (dResultStatus == "error")
                    {
                        dynamic dResultError = JObject.Parse(result.ToString())["error"].ToString();
                        Logger.Info($"Invitation status is {dResultStatus} - {dResultError}");
                    }
                    else
                    {
                        Logger.Info($"Invitation status is {dResultStatus}");
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error($"Unable to Send Invite : Error in SendInvitation {entityType} {entityId}", ex);
                return false;
            }
        }

        private void AddBranchIfNotExist(string branchName)
        {
            if (string.IsNullOrWhiteSpace(branchName))
                return;

            var branches = Lookup.GetLookupEntries("branch");

            if (branches != null && branches.Count > 0)
            {
                if (!branches.ContainsValue(branchName))
                    Lookup.Add("branch", new Dictionary<string, string> { { branchName, branchName } });
            }
        }

        private void EnsureFileTypeIsValid(PostedFileDetails file)
        {
            if (file == null)
            {
                throw new ArgumentException($"file not found");
            }

            string FileExtension = Path.GetExtension(file.FileName);

            if (!Array.Exists(Configuration.AcceptedFileIn, ext => ext == FileExtension))
            {
                throw new ArgumentException($"CSV file not found");
            }

            if (file.Content.Length > Configuration.MaxUploadFileSizeLimit * 1048576)
            {
                throw new ArgumentException($"file size should be less than {Configuration.MaxUploadFileSizeLimit} MB");
            }
        }

        private string EnsureEntityTypeIsValid(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (Lookup.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");
            return entityType;
        }

        private List<ExportedUserProfile> ReadCSVFile(Stream ms)
        {
            var csv = new CsvReader(new StreamReader(ms));
            csv.Configuration.WillThrowOnMissingField = false;
            csv.Configuration.IgnoreReferences = true;

            var records = csv.GetRecords<ExportedUserProfile>().ToList();
            return records;
        }

        public async Task<IIndividualMemberResponse> AddIndividualMember(string entityType, string entityId, UserProfile member)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            entityType = EnsureEntityTypeIsValid(entityType);

            IIndividualMemberResponse response = new IndividualMemberResponse();
            response.Member = member;

            try
            {
                await ImportUser(entityType, entityId, member);
            }
            catch (DuplicateUserFoundException)
            {
                response.Message = "Duplicate Member found";
            }
            catch (InvalidArgumentException)
            {
                response.Message = "Invalid details";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return response;
        }

        public async Task<ISocialPlatformModel> UpdateSocialPlatform(string username, SocialPlatformModel platform)
        {
            if (platform == null)
                throw new ArgumentNullException($"{platform} cannot be null");

            username = username.DocittUrlDecode();

            var userProfile = GetUserProfileByUserName(username);

            if (userProfile == null)
                throw new NotFoundException($"User {username} cannot be found");

            platform.Name = platform.Name.ToLower();

            if (platform.Name == SocialPlatforms.LinkedIn.ToString().ToLower())
                userProfile.LinkedInUrl = platform.URL;
            else if (platform.Name == SocialPlatforms.Twitter.ToString().ToLower())
                userProfile.TwitterUrl = platform.URL;
            else if (platform.Name == SocialPlatforms.Facebook.ToString().ToLower())
                userProfile.FacebookUrl = platform.URL;

            await UserProfileRepository.UpdateProfile(username, userProfile);
            await EventHubClient.Publish(new UserProfileModified() { UserProfile = userProfile });

            return platform;
        }
        public async Task<object> UpdateSettings(string username, string key, object value)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await GetProfile(username);
            if (user == null)
                throw new NotFoundException($"User {username} cannot be found");

            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var settings = JsonConvert.SerializeObject(value);

            user.Settings = AddOrUpdateUserSetting(user.Settings, key, settings);

            UserProfileRepository.Update(user);

            return value;
        }

        public async Task<object> GetSetting(string username, string key)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await GetProfile(username);
            if (user == null)
                throw new NotFoundException($"User {username} cannot be found");

            var settings = user.Settings;

            if (settings == null || !settings.Any() || !settings.ContainsKey(key))
            {
                throw new NotFoundException($"{key} setting not found");
            }

            return JsonConvert.DeserializeObject(settings[key].ToString());
        }

        public async Task<Dictionary<string, object>> GetAllSettings(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await GetProfile(username);
            if (user == null)
                throw new NotFoundException($"User {username} cannot be found");

            var settings = user.Settings;

            if (settings == null || !settings.Any())
            {
                throw new NotFoundException($"Settings not found");
            }

            var keys = new List<string>(settings.Keys);
            foreach (string key in keys)
            {
                settings[key] = JsonConvert.DeserializeObject(settings[key].ToString());
            }

            return settings;
        }

        public IUserProfile AssignUserProfile(IUserProfile existingProfile, IUserProfile updatedProfile)
        {
            existingProfile.FirstName = updatedProfile.FirstName != null ? updatedProfile.FirstName : existingProfile.FirstName;
            existingProfile.LastName = updatedProfile.LastName != null ? updatedProfile.LastName : existingProfile.LastName;
            existingProfile.CompanyName = updatedProfile.CompanyName != null ? updatedProfile.CompanyName : existingProfile.CompanyName;
            existingProfile.Title = updatedProfile.Title != null ? updatedProfile.Title : existingProfile.Title;
            existingProfile.License = updatedProfile.License != null ? updatedProfile.License : existingProfile.License;
            existingProfile.MobileNumber = updatedProfile.MobileNumber != null ? updatedProfile.MobileNumber : existingProfile.MobileNumber;
            existingProfile.OfficePhone = updatedProfile.OfficePhone != null ? updatedProfile.OfficePhone : existingProfile.OfficePhone;
            existingProfile.Phone = updatedProfile.Phone != null ? updatedProfile.Phone : existingProfile.Phone;
            existingProfile.Fax = updatedProfile.Fax != null ? updatedProfile.Fax : existingProfile.Fax;
            existingProfile.Email = updatedProfile.Email != null ? updatedProfile.Email : existingProfile.Email;
            existingProfile.Address = updatedProfile.Address != null ? updatedProfile.Address : existingProfile.Address;
            existingProfile.City = updatedProfile.City != null ? updatedProfile.City : existingProfile.City;
            existingProfile.State = updatedProfile.State != null ? updatedProfile.State : existingProfile.State;
            existingProfile.ZipCode = updatedProfile.ZipCode != null ? updatedProfile.ZipCode : existingProfile.ZipCode;
            existingProfile.LinkedInUrl = updatedProfile.LinkedInUrl != null ? updatedProfile.LinkedInUrl : existingProfile.LinkedInUrl;
            existingProfile.FacebookUrl = updatedProfile.FacebookUrl != null ? updatedProfile.FacebookUrl : existingProfile.FacebookUrl;
            existingProfile.TwitterUrl = updatedProfile.TwitterUrl != null ? updatedProfile.TwitterUrl : existingProfile.TwitterUrl;
            existingProfile.BranchName = updatedProfile.BranchName != null ? updatedProfile.BranchName : existingProfile.BranchName;
            existingProfile.Note = updatedProfile.Note != null ? updatedProfile.Note : existingProfile.Note;
            existingProfile.Communication = updatedProfile.Communication != null ? updatedProfile.Communication : existingProfile.Communication;
            existingProfile.NmlsNumber = updatedProfile.NmlsNumber;
            
            return existingProfile;
        } 

        public async Task<IUserProfile> GetProfile(string fname, string lname, string email)
        {
            Logger.Debug("Inside GetProfile...");
            if (string.IsNullOrWhiteSpace(fname))
                throw new ArgumentNullException(nameof(fname));

            if (string.IsNullOrWhiteSpace(lname))
                throw new ArgumentNullException(nameof(lname));

            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email));

            email = email.Trim().ToLower().DocittUrlDecode();

            var user = await UserProfileRepository.GetProfile(fname, lname, email);

            if (user == null)
                throw new NotFoundException($"User {fname} {lname} {email} cannot be found");
            user.BrandedSiteUrl = $"{Configuration.BorrowerPortal}{user.BrandedUniqueCode}";
            return user;
        }

        public async Task<List<IUserAutoSuggestion>> GetLoanOfficerList()
        {
            Logger.Debug("Inside GetLoanOfficerList...");
            var loanOfficerRole = Configuration.RoleNames.LoanOfficer;

            if(string.IsNullOrEmpty(loanOfficerRole))
                throw new Exception("LoanOfficer Role not defined in configuration");

            var users = await UserProfileRepository.GetUserListByTitle(loanOfficerRole);
            Logger.Debug("...Completed GetLoanOfficerList");
            return users.Where(x=> !string.IsNullOrEmpty(x.FirstName)).ToList();
        }
    }
}