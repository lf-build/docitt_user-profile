﻿using Docitt.UserProfile.Events;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Listener;
using LendFoundry.Security.Identity;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Docitt.UserProfile
{
    public class UserProfileListener : ListenerBase, IUserProfileListener
    {
        public UserProfileListener
        (
           IConfigurationServiceFactory configurationFactory,
           ITokenHandler tokenHandler,
           IEventHubClientFactory eventHubFactory,
           IUserProfileRepositoryFactory repositoryFactory,
           ILoggerFactory loggerFactory,
           ITenantServiceFactory tenantServiceFactory,
           IIdentityServiceFactory identityServiceFactory
        ) : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            if (configurationFactory == null) throw new ArgumentException($"{nameof(configurationFactory)} cannot be null");
            if (tokenHandler == null) throw new ArgumentException($"{nameof(tokenHandler)} cannot be null");
            if (eventHubFactory == null) throw new ArgumentException($"{nameof(eventHubFactory)} cannot be null");
            if (repositoryFactory == null) throw new ArgumentException($"{nameof(repositoryFactory)} cannot be null");
            if (loggerFactory == null) throw new ArgumentException($"{nameof(loggerFactory)} cannot be null");
            if (tenantServiceFactory == null) throw new ArgumentException($"{nameof(tenantServiceFactory)} cannot be null");
            if (identityServiceFactory == null) throw new ArgumentException($"{nameof(identityServiceFactory)} cannot be null");

            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
            RepositoryFactory = repositoryFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            IdentityServiceFactory = identityServiceFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IUserProfileRepositoryFactory RepositoryFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private IIdentityServiceFactory IdentityServiceFactory { get; }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            if (configuration == null)
            {
                return null;
            }
            else
            {
                return configuration.EventMappings.Select(x => x.EventName).Distinct().ToList();
            }
        }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            try
            {
                var token = TokenHandler.Issue(tenant, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);
                var eventhub = EventHubFactory.Create(reader);
                var repository = RepositoryFactory.Create(reader);
                var identityService = IdentityServiceFactory.Create(reader);

                var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
                configurationService.ClearCache(Settings.ServiceName);
                var configuration = configurationService.Get();

                if (configuration == null)
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                    return null;
                }
                else
                {
                    var eventMappings = configuration.EventMappings;

                    if (eventMappings == null)
                        throw new Exception("Event Mapping configuration not found ");

                    logger.Info($"#{eventMappings.Count} entity(ies) found from configuration: {Settings.ServiceName}");

                    eventMappings.ForEach(eventName =>
                    {
                        eventhub.On(eventName.EventName, UpdateUserProfile(repository, configuration, logger, EventHubFactory, identityService));
                        logger.Info($"It was made subscription to EventHub with the Event: #{eventName} for tenant {tenant}");
                    });
                    return eventMappings.Select(x => x.EventName).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Unable to subscribe event for ${tenant}", ex);
                return new List<string>();
            }
        }

        private static Action<EventInfo> UpdateUserProfile(IUserProfileRepository repository, Configuration configuration, ILogger logger, IEventHubClientFactory eventHubClientFactory, LendFoundry.Security.Identity.Client.IIdentityService identityService)
        {
            return @event =>
            {
                try
                {
                    logger.Info($"Processing {@event.Name} for {@event.TenantId} with Id - {@event.Id} ");

                    var eventMapping =
                        configuration.EventMappings.FirstOrDefault(e => string.Equals(e.EventName, @event.Name, StringComparison.CurrentCultureIgnoreCase));

                    if (eventMapping == null)
                        throw new Exception($"Event Mapping configuration not found for event : {@event.Name}");

                    LendFoundry.Security.Identity.IUser user = null;
                    var userName = eventMapping.Username.FormatWith(@event);
                    var hub = eventHubClientFactory.Create(new StaticTokenReader(@event.Token));
                    try
                    {
                        user = identityService.GetUser(userName).Result;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"User information not  found for user : {userName}", ex);
                    }
                    if (user != null)
                    {
                        if (!string.IsNullOrEmpty(user.Email))
                        {
                            var userProfile = repository.GetByUsername(user.Username).Result;

                            if (userProfile != null)
                            {
                                userProfile.Username = user.Username;
                                userProfile.UserId = user.Id;
                                //userProfile.BrandedUniqueCode = Guid.NewGuid().ToString();
                                repository.Update(userProfile);


                                hub.Publish(new UserProfileModified() { UserProfile = userProfile });
                            }
                            else
                            {
                                //logger.Info($"No data found for {user.Email}");
                                string role = string.Empty;
                                string[] expectRole = new string[1];
                                expectRole[0] = "{*any}";

                                if (user.Roles != null)
                                {
                                    if (user.Roles.Count() > 0)
                                    {
                                        var roles = user.Roles.Except(expectRole).ToArray();
                                        if (roles != null && roles.Count() > 0)
                                            role = roles[0];
                                    }
                                }
                                IUserProfile objProfile = new UserProfile();
                                objProfile.Email = user.Email;
                                objProfile.UserId = user.Id;
                                objProfile.Username = user.Username;
                                logger.Debug($"Userprofile adding titles");

                                objProfile.Title = role;
                                logger.Debug($"Userprofile updated titles {role}");
                                logger.Debug($"Userprofile adding roles");

                                if (user.Roles != null && user.Roles.Any(p => configuration.RolesForBrandedSite.Contains(p)))
                                {
                                    objProfile.BrandedUniqueCode = repository.GetBrandedUniqueCode().Result;
                                }
                                logger.Debug($"Userprofile adding Settings");
                                if (objProfile.Settings == null)
                                {
                                    objProfile.Settings = new Dictionary<string, object>();
                                    var usersetting = configuration.Settings;

                                    foreach (var setting in usersetting)
                                    {
                                        objProfile.Settings.Add(setting.Name, JsonConvert.SerializeObject(setting.DefaultValue));
                                    }
                                }

                                logger.Debug($"Creating UserProfile for {objProfile.Email} {objProfile.Title}");
                                repository.Add(objProfile);
                                hub.Publish(new UserProfileCreated() { UserProfile = objProfile });
                                logger.Debug($"Created UserProfile for {objProfile.Email} {objProfile.Title}");
                            }
                        }
                        else
                        {
                            logger.Info($"No data found for {@event.Name} ");
                        }
                    }
                    else
                        logger.Info($"No data found for {@event.Name} ");

                }
                catch (Exception ex)
                {
                    logger.Error($"Error while processing event #{@event.Name}. Error: {ex.Message}", ex, @event);
                }
            };
        }

    }
}