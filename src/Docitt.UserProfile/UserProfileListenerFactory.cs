﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace Docitt.UserProfile
{
    public static class UserProfileListenerFactory
    {
        public static void UseUserProfileListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IUserProfileListener>().Start();
        }
    }
}