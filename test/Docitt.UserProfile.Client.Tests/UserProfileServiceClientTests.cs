﻿using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;
using Xunit;

namespace Docitt.UserProfile.Client.Tests
{
    public class UserProfileServiceClientTests
    {
        private IUserProfileService Client { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> ServiceClient { get; }

        public UserProfileServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new UserProfileService(ServiceClient.Object);
        }

        [Fact]
        public async void Client_GetProfile()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<UserProfile>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new UserProfile());

            var result = await Client.GetProfile("suresh.b@sigmainfo.net");

            ServiceClient.Verify(x => x.ExecuteAsync<UserProfile>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{username}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_Add()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<UserProfile>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new UserProfile());
            var newUser = new UserProfile
            {
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };
            var result = await Client.AddProfile(newUser);

            ServiceClient.Verify(x => x.ExecuteAsync<UserProfile>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_Update()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<UserProfile>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new UserProfile());
            var username = "test123";
            var newUser = new UserProfile
            {
                Email = "suresh.b@sigmainfo.net",

                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };
            var result = await Client.UpdateProfile(username, newUser);

            ServiceClient.Verify(x => x.ExecuteAsync<UserProfile>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{username}", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
            Assert.NotNull(result);
        }
    }
}