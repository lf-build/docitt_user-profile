﻿using Docitt.UserProfile.Api.Controllers;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Mvc;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace Docitt.UserProfile.Api.Tests
{
    public class ApiControllerTests
    {
        private Mock<IConfiguration> Configuration { get; }
        private Mock<IUserProfileRepository> UserProfileRepository { get; set; }
        private Mock<IUserCompanyProfileRepository> UserCompanyProfileRepository { get; set; }
        private Mock<TenantTime> TenantTime { get; }
        private Mock<LendFoundry.Configuration.Client.IConfigurationServiceFactory> ConfigurationServiceFactory { get; }

        private Mock<IHelperService> HelpService { get; set; }

        private IUserProfileService Service { get; set; }
        private IUserCompanyProfileService CompanyService { get; set; }

        private Mock<ITokenReader> TokenReader { get; }
        private Mock<ILogger> Logger { get; }
        private ApiController apiController { get; set; }
        private Mock<IDocumentManagerService> DocumentManager { get; }

        private Mock<IEventHubClient> EventHubClient { get; }

        public ApiControllerTests()
        {
            var configuration = GetConfiguration();
            UserProfileRepository = new Mock<IUserProfileRepository>();
            UserCompanyProfileRepository = new Mock<IUserCompanyProfileRepository>();
            HelpService = new Mock<IHelperService>();
            Logger = new Mock<ILogger>();
            DocumentManager = new Mock<IDocumentManagerService>();
            EventHubClient = new Mock<IEventHubClient>();
            TenantTime = new Mock<TenantTime>();

            Service = new UserProfileService(
                configuration, 
                EventHubClient.Object, 
                UserProfileRepository.Object, 
                DocumentManager.Object,
                null,
                null,
                null,
                null);

            CompanyService = new UserCompanyProfileService(
                configuration,
                Logger.Object, 
                UserCompanyProfileRepository.Object, 
                UserProfileRepository.Object,
                HelpService.Object,
                EventHubClient.Object,
                TenantTime.Object);

            apiController = new ApiController(Service, Logger.Object, CompanyService);
        }

        private static ITokenHandler GetTokenHandler()
        {
            return new TokenHandler(new SecurityTokenConfig(), Mock.Of<ITokenReader>(),
                Mock.Of<ITokenWriter>());
        }

        private static Docitt.UserProfile.Configuration GetConfiguration()
        {
            const string configurationJson = "{											     " +
                                             "	\"sessionTimeout\": 10,												 " +
                                             "	\"tokenTimeout\": 60,                                                " +
                                             "}                                                                       ";
            var configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<Docitt.UserProfile.Configuration>(configurationJson);

            return configuration;
        }

        [Fact]
        public async void GetProfile_WithEmptyUserName_WithError()
        {
            var userName = "";
            var newUser = new UserProfile
            {
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                Email = "suresh.b@sigmainfo.net",
                MobileNumber = "9998931362",
                CompanyName = "test",
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                Address = "Test address",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.Get(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));
            var result = (ErrorResult)await apiController.GetProfile(userName);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void GetProfile_WithInvalidUserName_WithUserNotFoundError()
        {
            var userName = "abc";
            UserProfile newUser = null;

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.Get(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));

            var result = (ErrorResult)await apiController.GetProfile(userName);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void GetProfile_WithvalidUserName_WithSuccess()
        {
            var userName = "test123";
            var newUser = new UserProfile
            {
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));
            var result = (HttpOkObjectResult)await apiController.GetProfile(userName);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public async void AddProfile_WithEmptyPayload_WithError()
        {
            UserProfile profile = null;
            var result = (ErrorResult)await apiController.Add(profile);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void AddProfile_WithAlredyAddedUser_WithArgumentException()
        {
            var userName = "abc";
            var newUser = new UserProfile
            {
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(newUser.Username))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));

            var result = (ErrorResult)await apiController.Add(newUser);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void AddProfile_WithvalidUserName_WithSuccess()
        {
            var userName = "test1234";
            var newUser = new UserProfile
            {
                Title = "Manager",
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));
            var result = (HttpOkObjectResult)await apiController.Add(newUser);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public async void UpdateProfile_WithEmptyPayload_WithError()
        {
            UserProfile profile = null;
            var result = (ErrorResult)await apiController.Update(null, profile);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void UpdateProfile_WithInvalidUser_WithUserNotFoundException()
        {
            var userName = "abc";
            var newUser = new UserProfile
            {
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));

            var result = (ErrorResult)await apiController.Update(userName, newUser);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void UpdateProfile_WithvalidUser_WithSuccess()
        {
            var userName = "test123";
            var newUser = new UserProfile
            {
                Title = "Manager",
                Email = "suresh.b@sigmainfo.net",

                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));
            var result = (HttpOkObjectResult)await apiController.Update(userName, newUser);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
        }
    }
}