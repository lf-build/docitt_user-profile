﻿using LendFoundry.Configuration.Client;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Docitt.UserProfile.Tests
{
    public class UserProfileServiceTests
    {
        private Mock<IConfiguration> Configuration { get; }
        private Mock<IUserProfileRepository> UserProfileRepository { get; set; }
        private Mock<IConfigurationServiceFactory> ConfigurationServiceFactory { get; }
        private IUserProfileService Service { get; set; }

        private Mock<ITokenReader> TokenReader { get; }

        private Mock<IDocumentManagerService> DocumentManager { get; }
        private Mock<IEventHubClient> EventHubClient { get; }

        public UserProfileServiceTests()
        {
            var configuration = GetConfiguration();
            //UserRepository = new Mock<IUserRepository>();
            UserProfileRepository = new Mock<IUserProfileRepository>();
            TokenReader = new Mock<ITokenReader>();
            DocumentManager = new Mock<IDocumentManagerService>();
            EventHubClient = new Mock<IEventHubClient>();
            Service = new UserProfileService(configuration, EventHubClient.Object, UserProfileRepository.Object, DocumentManager.Object,null,null,null,null);
        }

        [Fact]
        public async void GetProfile_WithEmptyUserName_WithError()
        {
            var userName = "";
            var newUser = new UserProfile
            {
                Id = "58a45bf31272af38a0c595c0",
                TenantId = "my-tenant",

                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                Address = "Test address",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com"
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.Get(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));
            await Assert.ThrowsAsync<ArgumentNullException>(() => Service.GetProfile(userName));
        }

        [Fact]
        public async void GetProfile_WithInvalidUserName_WithUserNotFoundError()
        {
            var userName = "abc";
            UserProfile newUser = null;

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.Get(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));

            await Assert.ThrowsAsync<NotFoundException>(() => Service.GetProfile(userName));
        }

        [Fact]
        public async void GetProfile_WithvalidUserName_WithSuccess()
        {
            var userName = "test123";
            var newUser = new UserProfile
            {
                Id = "58a45bf31272af38a0c595c0",
                TenantId = "my-tenant",
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                Address = "Test address",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com"
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));
            var profile = await Service.GetProfile(userName);
            Assert.NotNull(profile);
        }

        private static ITokenHandler GetTokenHandler()
        {
            return new TokenHandler(new SecurityTokenConfig(), Mock.Of<ITokenReader>(),
                Mock.Of<ITokenWriter>());
        }

        private static Docitt.UserProfile.Configuration GetConfiguration()
        {
            const string configurationJson = "{											     " +
                                             "	\"sessionTimeout\": 10,												 " +
                                             "	\"tokenTimeout\": 60,                                                " +
                                             "}                                                                       ";
            var configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<Docitt.UserProfile.Configuration>(configurationJson);

            return configuration;
        }

        [Fact]
        public async void AddProfile_WithEmptyPayload_WithError()
        {
            UserProfile profile = null;
            await Assert.ThrowsAsync<InvalidArgumentException>(() => Service.AddProfile(profile));
        }

        [Fact]
        public async void AddProfile_WithAlredyAddedUser_WithArgumentException()
        {
            var userName = "abc";
            var newUser = new UserProfile
            {
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(newUser.Username))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));

            await Assert.ThrowsAsync<InvalidArgumentException>(() => Service.AddProfile(newUser));
        }

        [Fact]
        public async void AddProfile_WithvalidUserName_WithSuccess()
        {
            var userName = "test1234";
            var newUser = new UserProfile
            {
                Title = "Asd",
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));
            var profile = await Service.AddProfile(newUser);
            Assert.NotNull(profile);
        }

        [Fact]
        public async void UpdateProfile_WithEmptyPayload_WithError()
        {
            UserProfile profile = null;
            await Assert.ThrowsAsync<ArgumentNullException>(() => Service.UpdateProfile(null, profile));
        }

        [Fact]
        public async void UpdateProfile_WithInvalidUser_WithUserNotFoundException()
        {
            var userName = "abc";
            var newUser = new UserProfile
            {
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));

            await Assert.ThrowsAsync<InvalidArgumentException>(() => Service.UpdateProfile(userName, newUser));
        }

        [Fact]
        public async void UpdateProfile_WithvalidUser_WithSuccess()
        {
            var userName = "test123";
            var newUser = new UserProfile
            {
                Title = "MR",
                Email = "suresh.b@sigmainfo.net",
                Username = "test123",
                UserId = "58a45b5fd126fe0007baf8f8",
                MobileNumber = "9998931362",
                CompanyName = null,
                FirstName = "Suresh",
                LastName = "Bari",
                OfficePhone = "07912345",
                City = "Ahmedabad",
                State = "Gujarat",
                ZipCode = null,
                FacebookUrl = "www.facebook.com",
                TwitterUrl = "www.twitter.com",
                LinkedInUrl = "www.linkedin.com",
            };

            UserProfileRepository.Setup(s => s.Add(newUser));
            UserProfileRepository.Setup(s => s.GetByUsername(userName))
             .Returns(() => Task.FromResult<IUserProfile>(newUser));
            var profile = await Service.UpdateProfile(userName, newUser);
            Assert.NotNull(profile);
        }
    }
}